﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class FormulirRekamMedis
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.Label48 = New System.Windows.Forms.Label()
        Me.Label49 = New System.Windows.Forms.Label()
        Me.Label50 = New System.Windows.Forms.Label()
        Me.Label51 = New System.Windows.Forms.Label()
        Me.BBB = New System.Windows.Forms.TextBox()
        Me.PBB = New System.Windows.Forms.TextBox()
        Me.SBB = New System.Windows.Forms.TextBox()
        Me.BBL = New System.Windows.Forms.TextBox()
        Me.Label52 = New System.Windows.Forms.Label()
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.BATAL1 = New System.Windows.Forms.Button()
        Me.SIMPAN1 = New System.Windows.Forms.Button()
        Me.CETAK1 = New System.Windows.Forms.Button()
        Me.EDIT1 = New System.Windows.Forms.Button()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.ALERGI = New System.Windows.Forms.ComboBox()
        Me.ICD = New System.Windows.Forms.ComboBox()
        Me.BIAYA = New System.Windows.Forms.TextBox()
        Me.JK = New System.Windows.Forms.TextBox()
        Me.OBAT = New System.Windows.Forms.ComboBox()
        Me.Button4 = New System.Windows.Forms.Button()
        Me.Button3 = New System.Windows.Forms.Button()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Label30 = New System.Windows.Forms.Label()
        Me.TINDAKAN = New System.Windows.Forms.RichTextBox()
        Me.Label29 = New System.Windows.Forms.Label()
        Me.GIZI = New System.Windows.Forms.TextBox()
        Me.Label28 = New System.Windows.Forms.Label()
        Me.KIE = New System.Windows.Forms.RichTextBox()
        Me.Label24 = New System.Windows.Forms.Label()
        Me.Label25 = New System.Windows.Forms.Label()
        Me.Label26 = New System.Windows.Forms.Label()
        Me.Label27 = New System.Windows.Forms.Label()
        Me.D_BIDAN = New System.Windows.Forms.TextBox()
        Me.DOSIS = New System.Windows.Forms.TextBox()
        Me.BATAL = New System.Windows.Forms.Button()
        Me.SIMPAN = New System.Windows.Forms.Button()
        Me.CETAK = New System.Windows.Forms.Button()
        Me.Label23 = New System.Windows.Forms.Label()
        Me.RS = New System.Windows.Forms.ComboBox()
        Me.Label22 = New System.Windows.Forms.Label()
        Me.RUJUK = New System.Windows.Forms.ComboBox()
        Me.Label21 = New System.Windows.Forms.Label()
        Me.KASUS = New System.Windows.Forms.ComboBox()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.AGAMA = New System.Windows.Forms.TextBox()
        Me.ALAMAT = New System.Windows.Forms.TextBox()
        Me.NO_BPJS = New System.Windows.Forms.TextBox()
        Me.TGL_LAHIR = New System.Windows.Forms.TextBox()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.NADI = New System.Windows.Forms.TextBox()
        Me.BB = New System.Windows.Forms.TextBox()
        Me.TB = New System.Windows.Forms.TextBox()
        Me.SB = New System.Windows.Forms.TextBox()
        Me.TDARAH = New System.Windows.Forms.TextBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.ANAMNESIS = New System.Windows.Forms.RichTextBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.NAMA = New System.Windows.Forms.TextBox()
        Me.NAMA_KK = New System.Windows.Forms.TextBox()
        Me.NIK = New System.Windows.Forms.TextBox()
        Me.PEKERJAAN = New System.Windows.Forms.TextBox()
        Me.NO_RM = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Rkp = New System.Windows.Forms.ComboBox()
        Me.EDIT = New System.Windows.Forms.Button()
        Me.Panel1.SuspendLayout()
        Me.Panel2.SuspendLayout()
        Me.Panel3.SuspendLayout()
        Me.SuspendLayout()
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.Panel2)
        Me.Panel1.Controls.Add(Me.Panel3)
        Me.Panel1.Controls.Add(Me.Label16)
        Me.Panel1.Controls.Add(Me.ALERGI)
        Me.Panel1.Controls.Add(Me.ICD)
        Me.Panel1.Controls.Add(Me.BIAYA)
        Me.Panel1.Controls.Add(Me.JK)
        Me.Panel1.Controls.Add(Me.OBAT)
        Me.Panel1.Controls.Add(Me.Button4)
        Me.Panel1.Controls.Add(Me.Button3)
        Me.Panel1.Controls.Add(Me.Button2)
        Me.Panel1.Controls.Add(Me.Button1)
        Me.Panel1.Controls.Add(Me.Label30)
        Me.Panel1.Controls.Add(Me.TINDAKAN)
        Me.Panel1.Controls.Add(Me.Label29)
        Me.Panel1.Controls.Add(Me.GIZI)
        Me.Panel1.Controls.Add(Me.Label28)
        Me.Panel1.Controls.Add(Me.KIE)
        Me.Panel1.Controls.Add(Me.Label24)
        Me.Panel1.Controls.Add(Me.Label25)
        Me.Panel1.Controls.Add(Me.Label26)
        Me.Panel1.Controls.Add(Me.Label27)
        Me.Panel1.Controls.Add(Me.D_BIDAN)
        Me.Panel1.Controls.Add(Me.DOSIS)
        Me.Panel1.Controls.Add(Me.BATAL)
        Me.Panel1.Controls.Add(Me.SIMPAN)
        Me.Panel1.Controls.Add(Me.CETAK)
        Me.Panel1.Controls.Add(Me.Label23)
        Me.Panel1.Controls.Add(Me.RS)
        Me.Panel1.Controls.Add(Me.Label22)
        Me.Panel1.Controls.Add(Me.RUJUK)
        Me.Panel1.Controls.Add(Me.Label21)
        Me.Panel1.Controls.Add(Me.KASUS)
        Me.Panel1.Controls.Add(Me.Label10)
        Me.Panel1.Controls.Add(Me.Label17)
        Me.Panel1.Controls.Add(Me.Label18)
        Me.Panel1.Controls.Add(Me.Label19)
        Me.Panel1.Controls.Add(Me.Label20)
        Me.Panel1.Controls.Add(Me.AGAMA)
        Me.Panel1.Controls.Add(Me.ALAMAT)
        Me.Panel1.Controls.Add(Me.NO_BPJS)
        Me.Panel1.Controls.Add(Me.TGL_LAHIR)
        Me.Panel1.Controls.Add(Me.Label11)
        Me.Panel1.Controls.Add(Me.Label12)
        Me.Panel1.Controls.Add(Me.Label13)
        Me.Panel1.Controls.Add(Me.Label14)
        Me.Panel1.Controls.Add(Me.Label15)
        Me.Panel1.Controls.Add(Me.NADI)
        Me.Panel1.Controls.Add(Me.BB)
        Me.Panel1.Controls.Add(Me.TB)
        Me.Panel1.Controls.Add(Me.SB)
        Me.Panel1.Controls.Add(Me.TDARAH)
        Me.Panel1.Controls.Add(Me.Label9)
        Me.Panel1.Controls.Add(Me.Label8)
        Me.Panel1.Controls.Add(Me.ANAMNESIS)
        Me.Panel1.Controls.Add(Me.Label7)
        Me.Panel1.Controls.Add(Me.Label6)
        Me.Panel1.Controls.Add(Me.Label5)
        Me.Panel1.Controls.Add(Me.Label4)
        Me.Panel1.Controls.Add(Me.Label3)
        Me.Panel1.Controls.Add(Me.Label2)
        Me.Panel1.Controls.Add(Me.NAMA)
        Me.Panel1.Controls.Add(Me.NAMA_KK)
        Me.Panel1.Controls.Add(Me.NIK)
        Me.Panel1.Controls.Add(Me.PEKERJAAN)
        Me.Panel1.Controls.Add(Me.NO_RM)
        Me.Panel1.Controls.Add(Me.Label1)
        Me.Panel1.Controls.Add(Me.Rkp)
        Me.Panel1.Controls.Add(Me.EDIT)
        Me.Panel1.Location = New System.Drawing.Point(12, 6)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(889, 508)
        Me.Panel1.TabIndex = 73
        '
        'Panel2
        '
        Me.Panel2.Controls.Add(Me.Label48)
        Me.Panel2.Controls.Add(Me.Label49)
        Me.Panel2.Controls.Add(Me.Label50)
        Me.Panel2.Controls.Add(Me.Label51)
        Me.Panel2.Controls.Add(Me.BBB)
        Me.Panel2.Controls.Add(Me.PBB)
        Me.Panel2.Controls.Add(Me.SBB)
        Me.Panel2.Controls.Add(Me.BBL)
        Me.Panel2.Controls.Add(Me.Label52)
        Me.Panel2.Location = New System.Drawing.Point(0, 282)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(414, 119)
        Me.Panel2.TabIndex = 140
        '
        'Label48
        '
        Me.Label48.AutoSize = True
        Me.Label48.Location = New System.Drawing.Point(159, 86)
        Me.Label48.Name = "Label48"
        Me.Label48.Size = New System.Drawing.Size(66, 13)
        Me.Label48.TabIndex = 112
        Me.Label48.Text = "Suhu Badan"
        '
        'Label49
        '
        Me.Label49.AutoSize = True
        Me.Label49.Location = New System.Drawing.Point(159, 65)
        Me.Label49.Name = "Label49"
        Me.Label49.Size = New System.Drawing.Size(80, 13)
        Me.Label49.TabIndex = 109
        Me.Label49.Text = "Panjang Badan"
        '
        'Label50
        '
        Me.Label50.AutoSize = True
        Me.Label50.Location = New System.Drawing.Point(159, 44)
        Me.Label50.Name = "Label50"
        Me.Label50.Size = New System.Drawing.Size(66, 13)
        Me.Label50.TabIndex = 108
        Me.Label50.Text = "Berat Badan"
        '
        'Label51
        '
        Me.Label51.AutoSize = True
        Me.Label51.Location = New System.Drawing.Point(159, 23)
        Me.Label51.Name = "Label51"
        Me.Label51.Size = New System.Drawing.Size(83, 13)
        Me.Label51.TabIndex = 105
        Me.Label51.Text = "Berat Baru Lahir"
        '
        'BBB
        '
        Me.BBB.Location = New System.Drawing.Point(260, 44)
        Me.BBB.Name = "BBB"
        Me.BBB.Size = New System.Drawing.Size(117, 20)
        Me.BBB.TabIndex = 94
        '
        'PBB
        '
        Me.PBB.Location = New System.Drawing.Point(260, 65)
        Me.PBB.Name = "PBB"
        Me.PBB.Size = New System.Drawing.Size(117, 20)
        Me.PBB.TabIndex = 96
        '
        'SBB
        '
        Me.SBB.Location = New System.Drawing.Point(260, 86)
        Me.SBB.Name = "SBB"
        Me.SBB.Size = New System.Drawing.Size(117, 20)
        Me.SBB.TabIndex = 98
        '
        'BBL
        '
        Me.BBL.Location = New System.Drawing.Point(260, 23)
        Me.BBL.Name = "BBL"
        Me.BBL.Size = New System.Drawing.Size(117, 20)
        Me.BBL.TabIndex = 92
        '
        'Label52
        '
        Me.Label52.AutoSize = True
        Me.Label52.Location = New System.Drawing.Point(20, 23)
        Me.Label52.Name = "Label52"
        Me.Label52.Size = New System.Drawing.Size(92, 13)
        Me.Label52.TabIndex = 97
        Me.Label52.Text = "Pemeriksaan Fisik"
        '
        'Panel3
        '
        Me.Panel3.Controls.Add(Me.BATAL1)
        Me.Panel3.Controls.Add(Me.SIMPAN1)
        Me.Panel3.Controls.Add(Me.CETAK1)
        Me.Panel3.Controls.Add(Me.EDIT1)
        Me.Panel3.Location = New System.Drawing.Point(431, 447)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(367, 60)
        Me.Panel3.TabIndex = 141
        '
        'BATAL1
        '
        Me.BATAL1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.BATAL1.Location = New System.Drawing.Point(262, 19)
        Me.BATAL1.Name = "BATAL1"
        Me.BATAL1.Size = New System.Drawing.Size(75, 23)
        Me.BATAL1.TabIndex = 121
        Me.BATAL1.Text = "BATAL"
        Me.BATAL1.UseVisualStyleBackColor = True
        '
        'SIMPAN1
        '
        Me.SIMPAN1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.SIMPAN1.Location = New System.Drawing.Point(181, 19)
        Me.SIMPAN1.Name = "SIMPAN1"
        Me.SIMPAN1.Size = New System.Drawing.Size(75, 23)
        Me.SIMPAN1.TabIndex = 120
        Me.SIMPAN1.Text = "SIMPAN"
        Me.SIMPAN1.UseVisualStyleBackColor = True
        '
        'CETAK1
        '
        Me.CETAK1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.CETAK1.Location = New System.Drawing.Point(100, 19)
        Me.CETAK1.Name = "CETAK1"
        Me.CETAK1.Size = New System.Drawing.Size(75, 23)
        Me.CETAK1.TabIndex = 119
        Me.CETAK1.Text = "CETAK"
        Me.CETAK1.UseVisualStyleBackColor = True
        '
        'EDIT1
        '
        Me.EDIT1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.EDIT1.Location = New System.Drawing.Point(19, 19)
        Me.EDIT1.Name = "EDIT1"
        Me.EDIT1.Size = New System.Drawing.Size(75, 23)
        Me.EDIT1.TabIndex = 117
        Me.EDIT1.Text = "EDIT"
        Me.EDIT1.UseVisualStyleBackColor = True
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Location = New System.Drawing.Point(21, 445)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(33, 13)
        Me.Label16.TabIndex = 141
        Me.Label16.Text = "Alergi"
        '
        'ALERGI
        '
        Me.ALERGI.FormattingEnabled = True
        Me.ALERGI.Items.AddRange(New Object() {"Ada", "Tidak Ada"})
        Me.ALERGI.Location = New System.Drawing.Point(159, 442)
        Me.ALERGI.Name = "ALERGI"
        Me.ALERGI.Size = New System.Drawing.Size(217, 21)
        Me.ALERGI.TabIndex = 140
        '
        'ICD
        '
        Me.ICD.FormattingEnabled = True
        Me.ICD.Location = New System.Drawing.Point(563, 163)
        Me.ICD.Name = "ICD"
        Me.ICD.Size = New System.Drawing.Size(217, 21)
        Me.ICD.TabIndex = 101
        '
        'BIAYA
        '
        Me.BIAYA.Location = New System.Drawing.Point(564, 95)
        Me.BIAYA.Name = "BIAYA"
        Me.BIAYA.ReadOnly = True
        Me.BIAYA.Size = New System.Drawing.Size(217, 20)
        Me.BIAYA.TabIndex = 86
        '
        'JK
        '
        Me.JK.Location = New System.Drawing.Point(159, 144)
        Me.JK.Name = "JK"
        Me.JK.ReadOnly = True
        Me.JK.Size = New System.Drawing.Size(217, 20)
        Me.JK.TabIndex = 79
        '
        'OBAT
        '
        Me.OBAT.FormattingEnabled = True
        Me.OBAT.Items.AddRange(New Object() {" "})
        Me.OBAT.Location = New System.Drawing.Point(564, 206)
        Me.OBAT.Name = "OBAT"
        Me.OBAT.Size = New System.Drawing.Size(217, 21)
        Me.OBAT.TabIndex = 104
        '
        'Button4
        '
        Me.Button4.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.Button4.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button4.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button4.Location = New System.Drawing.Point(830, 164)
        Me.Button4.Name = "Button4"
        Me.Button4.Size = New System.Drawing.Size(29, 20)
        Me.Button4.TabIndex = 139
        Me.Button4.Text = ">"
        Me.Button4.TextAlign = System.Drawing.ContentAlignment.TopCenter
        Me.Button4.UseVisualStyleBackColor = False
        '
        'Button3
        '
        Me.Button3.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.Button3.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button3.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button3.Location = New System.Drawing.Point(830, 206)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(29, 20)
        Me.Button3.TabIndex = 138
        Me.Button3.Text = ">"
        Me.Button3.TextAlign = System.Drawing.ContentAlignment.TopCenter
        Me.Button3.UseVisualStyleBackColor = False
        '
        'Button2
        '
        Me.Button2.BackColor = System.Drawing.Color.Lime
        Me.Button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button2.Location = New System.Drawing.Point(795, 206)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(29, 20)
        Me.Button2.TabIndex = 105
        Me.Button2.Text = "+"
        Me.Button2.UseVisualStyleBackColor = False
        '
        'Button1
        '
        Me.Button1.BackColor = System.Drawing.Color.Lime
        Me.Button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.Location = New System.Drawing.Point(795, 164)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(29, 20)
        Me.Button1.TabIndex = 102
        Me.Button1.Text = "+"
        Me.Button1.UseVisualStyleBackColor = False
        '
        'Label30
        '
        Me.Label30.AutoSize = True
        Me.Label30.Location = New System.Drawing.Point(440, 346)
        Me.Label30.Name = "Label30"
        Me.Label30.Size = New System.Drawing.Size(52, 13)
        Me.Label30.TabIndex = 137
        Me.Label30.Text = "Tindakan"
        '
        'TINDAKAN
        '
        Me.TINDAKAN.Location = New System.Drawing.Point(564, 335)
        Me.TINDAKAN.Name = "TINDAKAN"
        Me.TINDAKAN.Size = New System.Drawing.Size(217, 42)
        Me.TINDAKAN.TabIndex = 111
        Me.TINDAKAN.Text = ""
        '
        'Label29
        '
        Me.Label29.AutoSize = True
        Me.Label29.Location = New System.Drawing.Point(440, 314)
        Me.Label29.Name = "Label29"
        Me.Label29.Size = New System.Drawing.Size(63, 13)
        Me.Label29.TabIndex = 136
        Me.Label29.Text = "Asupan Gizi"
        '
        'GIZI
        '
        Me.GIZI.Location = New System.Drawing.Point(564, 314)
        Me.GIZI.Name = "GIZI"
        Me.GIZI.Size = New System.Drawing.Size(217, 20)
        Me.GIZI.TabIndex = 110
        '
        'Label28
        '
        Me.Label28.AutoSize = True
        Me.Label28.Location = New System.Drawing.Point(440, 282)
        Me.Label28.Name = "Label28"
        Me.Label28.Size = New System.Drawing.Size(24, 13)
        Me.Label28.TabIndex = 135
        Me.Label28.Text = "KIE"
        '
        'KIE
        '
        Me.KIE.Location = New System.Drawing.Point(564, 271)
        Me.KIE.Name = "KIE"
        Me.KIE.Size = New System.Drawing.Size(217, 42)
        Me.KIE.TabIndex = 107
        Me.KIE.Text = ""
        '
        'Label24
        '
        Me.Label24.AutoSize = True
        Me.Label24.Location = New System.Drawing.Point(440, 227)
        Me.Label24.Name = "Label24"
        Me.Label24.Size = New System.Drawing.Size(33, 13)
        Me.Label24.TabIndex = 134
        Me.Label24.Text = "Dosis"
        '
        'Label25
        '
        Me.Label25.AutoSize = True
        Me.Label25.Location = New System.Drawing.Point(440, 206)
        Me.Label25.Name = "Label25"
        Me.Label25.Size = New System.Drawing.Size(33, 13)
        Me.Label25.TabIndex = 133
        Me.Label25.Text = "Obat "
        '
        'Label26
        '
        Me.Label26.AutoSize = True
        Me.Label26.Location = New System.Drawing.Point(440, 185)
        Me.Label26.Name = "Label26"
        Me.Label26.Size = New System.Drawing.Size(106, 13)
        Me.Label26.TabIndex = 132
        Me.Label26.Text = "Diagnosa Kebidanan"
        '
        'Label27
        '
        Me.Label27.AutoSize = True
        Me.Label27.Location = New System.Drawing.Point(440, 164)
        Me.Label27.Name = "Label27"
        Me.Label27.Size = New System.Drawing.Size(88, 13)
        Me.Label27.TabIndex = 131
        Me.Label27.Text = "Diagnosa ICD 10"
        '
        'D_BIDAN
        '
        Me.D_BIDAN.Location = New System.Drawing.Point(564, 185)
        Me.D_BIDAN.Name = "D_BIDAN"
        Me.D_BIDAN.Size = New System.Drawing.Size(217, 20)
        Me.D_BIDAN.TabIndex = 103
        '
        'DOSIS
        '
        Me.DOSIS.Location = New System.Drawing.Point(564, 228)
        Me.DOSIS.Name = "DOSIS"
        Me.DOSIS.Size = New System.Drawing.Size(217, 20)
        Me.DOSIS.TabIndex = 106
        '
        'BATAL
        '
        Me.BATAL.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.BATAL.Location = New System.Drawing.Point(686, 463)
        Me.BATAL.Name = "BATAL"
        Me.BATAL.Size = New System.Drawing.Size(75, 23)
        Me.BATAL.TabIndex = 121
        Me.BATAL.Text = "BATAL"
        Me.BATAL.UseVisualStyleBackColor = True
        '
        'SIMPAN
        '
        Me.SIMPAN.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.SIMPAN.Location = New System.Drawing.Point(605, 463)
        Me.SIMPAN.Name = "SIMPAN"
        Me.SIMPAN.Size = New System.Drawing.Size(75, 23)
        Me.SIMPAN.TabIndex = 120
        Me.SIMPAN.Text = "SIMPAN"
        Me.SIMPAN.UseVisualStyleBackColor = True
        '
        'CETAK
        '
        Me.CETAK.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.CETAK.Location = New System.Drawing.Point(524, 463)
        Me.CETAK.Name = "CETAK"
        Me.CETAK.Size = New System.Drawing.Size(75, 23)
        Me.CETAK.TabIndex = 119
        Me.CETAK.Text = "CETAK"
        Me.CETAK.UseVisualStyleBackColor = True
        '
        'Label23
        '
        Me.Label23.AutoSize = True
        Me.Label23.Location = New System.Drawing.Point(686, 396)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(68, 13)
        Me.Label23.TabIndex = 129
        Me.Label23.Text = "Rumah Sakit"
        '
        'RS
        '
        Me.RS.FormattingEnabled = True
        Me.RS.Location = New System.Drawing.Point(764, 393)
        Me.RS.Name = "RS"
        Me.RS.Size = New System.Drawing.Size(106, 21)
        Me.RS.TabIndex = 116
        '
        'Label22
        '
        Me.Label22.AutoSize = True
        Me.Label22.Location = New System.Drawing.Point(440, 423)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(35, 13)
        Me.Label22.TabIndex = 128
        Me.Label22.Text = "Rujuk"
        '
        'RUJUK
        '
        Me.RUJUK.FormattingEnabled = True
        Me.RUJUK.Items.AddRange(New Object() {"YA", "TIDAK"})
        Me.RUJUK.Location = New System.Drawing.Point(564, 420)
        Me.RUJUK.Name = "RUJUK"
        Me.RUJUK.Size = New System.Drawing.Size(106, 21)
        Me.RUJUK.TabIndex = 115
        '
        'Label21
        '
        Me.Label21.AutoSize = True
        Me.Label21.Location = New System.Drawing.Point(440, 396)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(63, 13)
        Me.Label21.TabIndex = 127
        Me.Label21.Text = "Jenis Kasus"
        '
        'KASUS
        '
        Me.KASUS.FormattingEnabled = True
        Me.KASUS.Items.AddRange(New Object() {"Baru ", "Lama", "KKL"})
        Me.KASUS.Location = New System.Drawing.Point(564, 393)
        Me.KASUS.Name = "KASUS"
        Me.KASUS.Size = New System.Drawing.Size(106, 21)
        Me.KASUS.TabIndex = 114
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(440, 116)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(53, 13)
        Me.Label10.TabIndex = 126
        Me.Label10.Text = "No. BPJS"
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Location = New System.Drawing.Point(440, 95)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(33, 13)
        Me.Label17.TabIndex = 125
        Me.Label17.Text = "Biaya"
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.Location = New System.Drawing.Point(440, 74)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(39, 13)
        Me.Label18.TabIndex = 124
        Me.Label18.Text = "Alamat"
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.Location = New System.Drawing.Point(440, 53)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(40, 13)
        Me.Label19.TabIndex = 123
        Me.Label19.Text = "Agama"
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.Location = New System.Drawing.Point(440, 32)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(72, 13)
        Me.Label20.TabIndex = 122
        Me.Label20.Text = "Tanggal Lahir"
        '
        'AGAMA
        '
        Me.AGAMA.Location = New System.Drawing.Point(564, 53)
        Me.AGAMA.Name = "AGAMA"
        Me.AGAMA.ReadOnly = True
        Me.AGAMA.Size = New System.Drawing.Size(217, 20)
        Me.AGAMA.TabIndex = 82
        '
        'ALAMAT
        '
        Me.ALAMAT.Location = New System.Drawing.Point(564, 74)
        Me.ALAMAT.Name = "ALAMAT"
        Me.ALAMAT.ReadOnly = True
        Me.ALAMAT.Size = New System.Drawing.Size(217, 20)
        Me.ALAMAT.TabIndex = 85
        '
        'NO_BPJS
        '
        Me.NO_BPJS.Location = New System.Drawing.Point(564, 117)
        Me.NO_BPJS.Name = "NO_BPJS"
        Me.NO_BPJS.ReadOnly = True
        Me.NO_BPJS.Size = New System.Drawing.Size(217, 20)
        Me.NO_BPJS.TabIndex = 88
        '
        'TGL_LAHIR
        '
        Me.TGL_LAHIR.Location = New System.Drawing.Point(564, 32)
        Me.TGL_LAHIR.Name = "TGL_LAHIR"
        Me.TGL_LAHIR.ReadOnly = True
        Me.TGL_LAHIR.Size = New System.Drawing.Size(217, 20)
        Me.TGL_LAHIR.TabIndex = 81
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(158, 377)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(66, 13)
        Me.Label11.TabIndex = 113
        Me.Label11.Text = "Suhu Badan"
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(158, 356)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(70, 13)
        Me.Label12.TabIndex = 112
        Me.Label12.Text = "Tinggi Badan"
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Location = New System.Drawing.Point(158, 335)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(66, 13)
        Me.Label13.TabIndex = 109
        Me.Label13.Text = "Berat Badan"
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Location = New System.Drawing.Point(158, 314)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(29, 13)
        Me.Label14.TabIndex = 108
        Me.Label14.Text = "Nadi"
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Location = New System.Drawing.Point(158, 293)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(82, 13)
        Me.Label15.TabIndex = 105
        Me.Label15.Text = "Tekanan Darah"
        '
        'NADI
        '
        Me.NADI.Location = New System.Drawing.Point(259, 314)
        Me.NADI.Name = "NADI"
        Me.NADI.Size = New System.Drawing.Size(117, 20)
        Me.NADI.TabIndex = 94
        '
        'BB
        '
        Me.BB.Location = New System.Drawing.Point(259, 335)
        Me.BB.Name = "BB"
        Me.BB.Size = New System.Drawing.Size(117, 20)
        Me.BB.TabIndex = 96
        '
        'TB
        '
        Me.TB.Location = New System.Drawing.Point(259, 356)
        Me.TB.Name = "TB"
        Me.TB.Size = New System.Drawing.Size(117, 20)
        Me.TB.TabIndex = 98
        '
        'SB
        '
        Me.SB.Location = New System.Drawing.Point(259, 377)
        Me.SB.Name = "SB"
        Me.SB.Size = New System.Drawing.Size(117, 20)
        Me.SB.TabIndex = 99
        '
        'TDARAH
        '
        Me.TDARAH.Location = New System.Drawing.Point(259, 293)
        Me.TDARAH.Name = "TDARAH"
        Me.TDARAH.Size = New System.Drawing.Size(117, 20)
        Me.TDARAH.TabIndex = 92
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(19, 293)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(92, 13)
        Me.Label9.TabIndex = 97
        Me.Label9.Text = "Pemeriksaan Fisik"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(19, 235)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(58, 13)
        Me.Label8.TabIndex = 95
        Me.Label8.Text = "Anamnesis"
        '
        'ANAMNESIS
        '
        Me.ANAMNESIS.Location = New System.Drawing.Point(159, 206)
        Me.ANAMNESIS.Name = "ANAMNESIS"
        Me.ANAMNESIS.Size = New System.Drawing.Size(217, 70)
        Me.ANAMNESIS.TabIndex = 90
        Me.ANAMNESIS.Text = ""
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(19, 167)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(55, 13)
        Me.Label7.TabIndex = 93
        Me.Label7.Text = "Pekerjaan"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(19, 144)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(71, 13)
        Me.Label6.TabIndex = 91
        Me.Label6.Text = "Jenis Kelamin"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(19, 123)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(25, 13)
        Me.Label5.TabIndex = 89
        Me.Label5.Text = "NIK"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(19, 102)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(52, 13)
        Me.Label4.TabIndex = 87
        Me.Label4.Text = "Nama KK"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(19, 81)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(35, 13)
        Me.Label3.TabIndex = 84
        Me.Label3.Text = "Nama"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(19, 60)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(92, 13)
        Me.Label2.TabIndex = 83
        Me.Label2.Text = "No. Rekam Medis"
        '
        'NAMA
        '
        Me.NAMA.Location = New System.Drawing.Point(159, 81)
        Me.NAMA.Name = "NAMA"
        Me.NAMA.ReadOnly = True
        Me.NAMA.Size = New System.Drawing.Size(217, 20)
        Me.NAMA.TabIndex = 76
        '
        'NAMA_KK
        '
        Me.NAMA_KK.Location = New System.Drawing.Point(159, 102)
        Me.NAMA_KK.Name = "NAMA_KK"
        Me.NAMA_KK.ReadOnly = True
        Me.NAMA_KK.Size = New System.Drawing.Size(217, 20)
        Me.NAMA_KK.TabIndex = 77
        '
        'NIK
        '
        Me.NIK.Location = New System.Drawing.Point(159, 123)
        Me.NIK.Name = "NIK"
        Me.NIK.ReadOnly = True
        Me.NIK.Size = New System.Drawing.Size(217, 20)
        Me.NIK.TabIndex = 78
        '
        'PEKERJAAN
        '
        Me.PEKERJAAN.Location = New System.Drawing.Point(159, 166)
        Me.PEKERJAAN.Name = "PEKERJAAN"
        Me.PEKERJAAN.ReadOnly = True
        Me.PEKERJAAN.Size = New System.Drawing.Size(217, 20)
        Me.PEKERJAAN.TabIndex = 80
        '
        'NO_RM
        '
        Me.NO_RM.Location = New System.Drawing.Point(159, 60)
        Me.NO_RM.Name = "NO_RM"
        Me.NO_RM.ReadOnly = True
        Me.NO_RM.Size = New System.Drawing.Size(217, 20)
        Me.NO_RM.TabIndex = 75
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(19, 25)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(134, 13)
        Me.Label1.TabIndex = 74
        Me.Label1.Text = "Riwayat Kunjungan Pasien"
        '
        'Rkp
        '
        Me.Rkp.FormattingEnabled = True
        Me.Rkp.Location = New System.Drawing.Point(159, 22)
        Me.Rkp.Name = "Rkp"
        Me.Rkp.Size = New System.Drawing.Size(217, 21)
        Me.Rkp.TabIndex = 73
        '
        'EDIT
        '
        Me.EDIT.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.EDIT.Location = New System.Drawing.Point(443, 463)
        Me.EDIT.Name = "EDIT"
        Me.EDIT.Size = New System.Drawing.Size(75, 23)
        Me.EDIT.TabIndex = 117
        Me.EDIT.Text = "EDIT"
        Me.EDIT.UseVisualStyleBackColor = True
        '
        'FormulirRekamMedis
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.ClientSize = New System.Drawing.Size(913, 533)
        Me.Controls.Add(Me.Panel1)
        Me.Name = "FormulirRekamMedis"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Formulir Rekam Medis"
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        Me.Panel3.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents Panel1 As Panel
    Friend WithEvents BATAL As Button
    Friend WithEvents SIMPAN As Button
    Friend WithEvents CETAK As Button
    Friend WithEvents Label11 As Label
    Friend WithEvents Label12 As Label
    Friend WithEvents Label13 As Label
    Friend WithEvents Label14 As Label
    Friend WithEvents Label15 As Label
    Friend WithEvents NADI As TextBox
    Friend WithEvents BB As TextBox
    Friend WithEvents TB As TextBox
    Friend WithEvents SB As TextBox
    Friend WithEvents TDARAH As TextBox
    Friend WithEvents Label9 As Label
    Friend WithEvents EDIT As Button
    Friend WithEvents Panel2 As Panel
    Friend WithEvents Label48 As Label
    Friend WithEvents Label49 As Label
    Friend WithEvents Label50 As Label
    Friend WithEvents Label51 As Label
    Friend WithEvents BBB As TextBox
    Friend WithEvents PBB As TextBox
    Friend WithEvents SBB As TextBox
    Friend WithEvents BBL As TextBox
    Friend WithEvents Label52 As Label
    Friend WithEvents ICD As ComboBox
    Friend WithEvents BIAYA As TextBox
    Friend WithEvents JK As TextBox
    Friend WithEvents OBAT As ComboBox
    Friend WithEvents Button4 As Button
    Friend WithEvents Button3 As Button
    Friend WithEvents Button2 As Button
    Friend WithEvents Button1 As Button
    Friend WithEvents Label30 As Label
    Friend WithEvents TINDAKAN As RichTextBox
    Friend WithEvents Label29 As Label
    Friend WithEvents GIZI As TextBox
    Friend WithEvents Label28 As Label
    Friend WithEvents KIE As RichTextBox
    Friend WithEvents Label24 As Label
    Friend WithEvents Label25 As Label
    Friend WithEvents Label26 As Label
    Friend WithEvents Label27 As Label
    Friend WithEvents D_BIDAN As TextBox
    Friend WithEvents DOSIS As TextBox
    Friend WithEvents Label23 As Label
    Friend WithEvents RS As ComboBox
    Friend WithEvents Label22 As Label
    Friend WithEvents RUJUK As ComboBox
    Friend WithEvents Label21 As Label
    Friend WithEvents KASUS As ComboBox
    Friend WithEvents Label10 As Label
    Friend WithEvents Label17 As Label
    Friend WithEvents Label18 As Label
    Friend WithEvents Label19 As Label
    Friend WithEvents Label20 As Label
    Friend WithEvents AGAMA As TextBox
    Friend WithEvents ALAMAT As TextBox
    Friend WithEvents NO_BPJS As TextBox
    Friend WithEvents TGL_LAHIR As TextBox
    Friend WithEvents Label8 As Label
    Friend WithEvents ANAMNESIS As RichTextBox
    Friend WithEvents Label7 As Label
    Friend WithEvents Label6 As Label
    Friend WithEvents Label5 As Label
    Friend WithEvents Label4 As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents NAMA As TextBox
    Friend WithEvents NAMA_KK As TextBox
    Friend WithEvents NIK As TextBox
    Friend WithEvents PEKERJAAN As TextBox
    Friend WithEvents NO_RM As TextBox
    Friend WithEvents Label1 As Label
    Friend WithEvents Rkp As ComboBox
    Friend WithEvents Label16 As Label
    Friend WithEvents ALERGI As ComboBox
    Friend WithEvents Panel3 As Panel
    Friend WithEvents BATAL1 As Button
    Friend WithEvents SIMPAN1 As Button
    Friend WithEvents CETAK1 As Button
    Friend WithEvents EDIT1 As Button
End Class
