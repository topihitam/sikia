﻿Imports MySql.Data.MySqlClient

Public Class Login
    Private Sub Button1_Click_1(sender As Object, e As EventArgs) Handles Button1.Click
        Call koneksi()
        Dim table As New DataTable()
        If session_level = "user" Then
            cmd = New MySqlCommand("select * from petugas where USERNAME_P = '" & user.Text & "' and PASSWORD_P = '" & pass.Text & "' and level != 'admin'", conn)
        ElseIf session_level = "admin" Then
            cmd = New MySqlCommand("select * from petugas where USERNAME_P = '" & user.Text & "' and PASSWORD_P = '" & pass.Text & "' and level = 'admin' ", conn)
        End If
        adapter = New MySqlDataAdapter(cmd)
        adapter.Fill(table)
        rd = cmd.ExecuteReader
        rd.Read()
        If rd.HasRows Then
            If rd.Item("level") = "admin" Then
                session_user = rd.Item("ID_PETUGAS")
                rd.Close()
                MenuKIA.Show()
                Me.Close()
            Else
                If rd.Item("JENIS_PETUGAS") = "POLI GIGI" Then
                    MessageBox.Show("Menu GIGI belum tersedia!")
                ElseIf rd.Item("JENIS_PETUGAS") = "POLI KIA" Then
                    session_user = rd.Item("ID_PETUGAS")
                    session_level = rd.Item("level")
                    session_poli = rd.Item("JENIS_PETUGAS")
                    rd.Close()
                    MenuKIA.Show()
                    Me.Close()
                End If
            End If
        Else
            MessageBox.Show("username atau password salah!")
        End If
    End Sub

    Private Sub Button2_Click_1(sender As Object, e As EventArgs) Handles Button2.Click
        user.Clear()
        pass.Clear()
        Panel1.Visible = False
        Panel3.Visible = True
    End Sub

    Private Sub Button6_Click(sender As Object, e As EventArgs) Handles Button6.Click
        session_level = "admin"
        Panel1.Visible = True
        Panel3.Visible = False
    End Sub

    Private Sub Button5_Click(sender As Object, e As EventArgs) Handles Button5.Click
        session_level = "user"
        Panel1.Visible = True
        Panel3.Visible = False
    End Sub
End Class