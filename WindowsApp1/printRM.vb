﻿Imports CrystalDecisions.CrystalReports.Engine
Imports System.ComponentModel
Imports MySql.Data.MySqlClient

Public Class printRM
    Public kode As String
    Private Sub CrystalReportViewer1_Load(sender As Object, e As EventArgs) Handles CrystalReportViewer1.Load
        Dim report As New ReportDocument()
        If kode = "bayi" Then
            report.Load(System.Windows.Forms.Application.StartupPath + "\CrystalReport3.rpt")
        Else
            report.Load(System.Windows.Forms.Application.StartupPath + "\CrystalReport1.rpt")
        End If

        Call koneksi()
        Dim table As New DataTable()
        cmd = New MySqlCommand("select * from pasien a join pelayanan b join poli_kia c where a.NO_RM = b.NO_RM and a.NO_RM = '" & session_NO_RM & "' and b.TGL = '" & session_TGL & "' and b.NOREG = c.NOREG", conn)
        adapter = New MySqlDataAdapter(cmd)
        adapter.Fill(table)
        report.SetDataSource(table)
        CrystalReportViewer1.ReportSource = report
        adapter.Dispose()
        cmd = New MySqlCommand("select * from detail_obatkia join poli_kia where detail_obatkia.id_KIA = poli_kia.id_KIA AND detail_obatkia.id_KIA = '" & session_idkia & "'", conn)
        adapter = New MySqlDataAdapter(cmd)
        adapter.Fill(table)
        report.Subreports.Item("obat").SetDataSource(table)
    End Sub
End Class