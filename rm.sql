SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

CREATE SCHEMA IF NOT EXISTS `rm_db` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci ;
USE `rm_db` ;

-- -----------------------------------------------------
-- Table `rm_db`.`petugas`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `rm_db`.`petugas` (
  `id_petugas` INT NOT NULL AUTO_INCREMENT,
  `nama_petugas` VARCHAR(45) NULL,
  `email` VARCHAR(45) NULL,
  `password` VARCHAR(45) NULL,
  PRIMARY KEY (`id_petugas`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `rm_db`.`poli`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `rm_db`.`poli` (
  `id_poli` INT NOT NULL AUTO_INCREMENT,
  `nama_poli` VARCHAR(45) NULL,
  PRIMARY KEY (`id_poli`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `rm_db`.`pasien`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `rm_db`.`pasien` (
  `id_pasien` VARCHAR(45) NOT NULL,
  `no_rm` VARCHAR(45) NULL,
  `nik` VARCHAR(45) NULL,
  `nama_pasien` VARCHAR(45) NULL,
  `jeniskelamin` VARCHAR(45) NULL,
  `alamat` VARCHAR(100) NULL,
  `poli_id_poli` INT NOT NULL,
  PRIMARY KEY (`id_pasien`),
  INDEX `fk_pasien_poli1_idx` (`poli_id_poli` ASC),
  CONSTRAINT `fk_pasien_poli1`
    FOREIGN KEY (`poli_id_poli`)
    REFERENCES `rm_db`.`poli` (`id_poli`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `rm_db`.`distributor`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `rm_db`.`distributor` (
  `id_distributor` INT NOT NULL AUTO_INCREMENT,
  `nama_distributor` VARCHAR(45) NULL,
  `no_telp` VARCHAR(45) NULL,
  PRIMARY KEY (`id_distributor`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `rm_db`.`pesan`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `rm_db`.`pesan` (
  `id_pesan` INT NOT NULL AUTO_INCREMENT,
  `isi_pesan` VARCHAR(45) NULL,
  PRIMARY KEY (`id_pesan`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `rm_db`.`peminjaman`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `rm_db`.`peminjaman` (
  `id_peminjaman` INT NOT NULL AUTO_INCREMENT,
  `tgl_peminjaman` VARCHAR(45) NULL,
  `keterangan` VARCHAR(45) NULL,
  `poli_id_poli` INT NOT NULL,
  `distributor_id_distributor` INT NOT NULL,
  `pasien_id_pasien` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id_peminjaman`),
  INDEX `fk_peminjaman_poli1_idx` (`poli_id_poli` ASC),
  INDEX `fk_peminjaman_distributor1_idx` (`distributor_id_distributor` ASC),
  INDEX `fk_peminjaman_pasien1_idx` (`pasien_id_pasien` ASC),
  CONSTRAINT `fk_peminjaman_poli1`
    FOREIGN KEY (`poli_id_poli`)
    REFERENCES `rm_db`.`poli` (`id_poli`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_peminjaman_distributor1`
    FOREIGN KEY (`distributor_id_distributor`)
    REFERENCES `rm_db`.`distributor` (`id_distributor`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_peminjaman_pasien1`
    FOREIGN KEY (`pasien_id_pasien`)
    REFERENCES `rm_db`.`pasien` (`id_pasien`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `rm_db`.`pengembalian`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `rm_db`.`pengembalian` (
  `id_pengembalian` INT NOT NULL AUTO_INCREMENT,
  `tgl_pengembalian` VARCHAR(45) NULL,
  `keterangan` VARCHAR(45) NULL,
  `poli_id_poli` INT NOT NULL,
  `pasien_id_pasien` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id_pengembalian`),
  INDEX `fk_pengembalian_poli_idx` (`poli_id_poli` ASC),
  INDEX `fk_pengembalian_pasien1_idx` (`pasien_id_pasien` ASC),
  CONSTRAINT `fk_pengembalian_poli`
    FOREIGN KEY (`poli_id_poli`)
    REFERENCES `rm_db`.`poli` (`id_poli`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_pengembalian_pasien1`
    FOREIGN KEY (`pasien_id_pasien`)
    REFERENCES `rm_db`.`pasien` (`id_pasien`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
